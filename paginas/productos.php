<h3>Listado de productos en la web</h3>
<br>
<section class="row">
	<?php  
	$sql="SELECT * FROM productos";
	$consulta=mysqli_query($conexion, $sql);
	while($r=mysqli_fetch_array($consulta)){
		?>
		<article class="col-md-3 col-sm-6" style="min-height:220px;">
			<header>
				<a href="index.php?p=detalle.php&id=<?php echo $r['id'];?>">
					<?php echo $r['nombre']; ?>
				</a>
			</header>
			<section>
				<?php echo $r['descripcion']; ?>
			</section>
			<footer>
				<?php echo $r['precio']; ?>
			</footer>
		</article>
		<?php
	}
	?>
</section>