<h3>
	Detalle de producto
	-
	<small>
	<a href="index.php">Volver</a>
	</small>
</h3>
<br>
<?php  
$id=$_GET['id'];
$sql="SELECT * FROM productos WHERE id=$id";
$consulta=mysqli_query($conexion, $sql);
$r=mysqli_fetch_array($consulta);
$iva=$r['precio']*0.21;
?>
<div class="jumbotron">
  <div class="container">
    <h1><?php echo $r['nombre'];?></h1>
    <p><?php echo $r['descripcion'];?></p>
    <h5><?php echo $r['precio'];?> Euros (+ <?php echo $iva;?> Euros de iva)</h5>
  </div>
</div>
<hr>

<?php  
if(isset($_POST['insertar'])){
	$nombre=$_POST['nombre'];
	$archivo=$_FILES['archivo']['name'];
	move_uploaded_file($_FILES['archivo']['tmp_name'], 'img/'.$archivo);

	$sql="INSERT INTO imagenes(nombre, archivo, idPro)VALUES('$nombre', '$archivo', $id)";
	$consulta=mysqli_query($conexion, $sql);
}
?>


<form action="index.php?p=detalle.php&id=<?php echo $id;?>" method="post" enctype="multipart/form-data">
	<input type="text" name="nombre">
	<input type="file" name="archivo">
	<input type="submit" value="insertar" name="insertar">
</form>

<section class="row">
	<?php  
	$sql="SELECT * FROM imagenes WHERE idPro=$id";
	$consulta=mysqli_query($conexion, $sql);
	while($r=mysqli_fetch_array($consulta)){
		?>
		<div class="col-md-2 col-sm-3">
			<img src="img/<?php echo $r['archivo'];?>" alt="" class="img-responsive">
		</div>
		<?php
	}
	?>
</section>

<hr>

<h4>Comentarios sobre el producto</h4>

<?php  
//Insertar un comentario
if(isset($_POST['comentar'])){
	$autor=$_POST['autor'];
	$texto=$_POST['texto'];

	$sql="INSERT INTO comentarios(autor, texto, idProd)VALUES('$autor', '$texto', $id)";

	$consulta=mysqli_query($conexion, $sql);
}
?>

<form action="index.php?p=detalle.php&id=<?php echo $id;?>" method="post" enctype="multipart/form-data">
	<input type="text" name="autor">
	<textarea name="texto" cols="30" rows="10"></textarea>
	<input type="submit" value="comentar" name="comentar">
</form>

<section class="row">
	<?php  
	$sql="SELECT * FROM comentarios WHERE idProd=$id";
	$consulta=mysqli_query($conexion, $sql);
	while($r=mysqli_fetch_array($consulta)){
		?>
		<div class="col-md-2 col-sm-3">
			<article>
				<?php echo $r['texto'];?>
			</article>
		</div>
		<?php
	}
	?>
</section>
