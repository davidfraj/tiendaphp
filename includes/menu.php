<?php  
if($_SESSION['conectado']){

	if($_SESSION['usuario']['tipoUsu']=='administrador'){
		$urlEnlace=array('productos.php', 'usuarios.php', 'contacto.php', 'galeria.php');
		$nomEnlace=array('Inicio', 'Dar de alta un usuario', 'Contacto', 'Galeria de imagenes');
	}else{
		$urlEnlace=array('productos.php', 'contacto.php', 'galeria.php');
		$nomEnlace=array('Inicio', 'Contacto', 'Galeria de imagenes');
	}

}else{

	$urlEnlace=array('productos.php');
	$nomEnlace=array('Inicio');

}

?>
<ul class="nav nav-tabs">
	<?php  
	for ($i=0; $i < count($urlEnlace); $i++) { 
		if($urlEnlace[$i]==$p){
			$cla='active';
		}else{
			$cla='';
		}
		?>
		<li class="<?php echo $cla;?>">
			<a href="index.php?p=<?php echo $urlEnlace[$i]; ?>">
				<?php echo $nomEnlace[$i]; ?>
			</a>
		</li>
		<?php
	}
	?>
</ul>