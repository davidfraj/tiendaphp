<?php  
//Esto es obligatorio, para no sacar fallos
//Comprobamos si existe una variable de session
if(!isset($_SESSION['conectado'])){
	$_SESSION['conectado']=null;
}

if(!isset($_SESSION['usuario'])){
	$_SESSION['usuario']=null;
}

//Compruebo las cookies de usuario. Si el usuario tiene una cookie con su correo, lo conectare con la variable de session adecuada
//Y de paso, renovare la cookie
if(isset($_COOKIE['correoUsu'])){
	$correoUsu=$_COOKIE['correoUsu'];
	$sql="SELECT * FROM usuarios WHERE correoUsu='$correoUsu'";
	$consulta=mysqli_query($conexion, $sql);
	if($r=mysqli_fetch_array($consulta)){
		$_SESSION['conectado']=true;
		$_SESSION['usuario']=$r;
		setcookie('correoUsu', $correoUsu, time()+(60*60*24*7));
	}
}

//Comprobamos si el usuario quiere conectarse
if(isset($_POST['conectar'])){
	$usuario=$_POST['usuario'];
	$clave=md5($_POST['clave']);

	$sql="SELECT * FROM usuarios WHERE correoUsu='$usuario' AND claveUsu='$clave'";
	$consulta=mysqli_query($conexion, $sql);
	if($r=mysqli_fetch_array($consulta)){
		$_SESSION['conectado']=true;
		$_SESSION['usuario']=$r;

		//Un checkBox, se recoge SIEMPRE con isset
		if(isset($_POST['mantener'])){
			//Crea un cookie, llamada correoUsu, que
			//durara en el ordenador del cliente, una semana
			//en segundos
			setcookie('correoUsu', $usuario, time()+(60*60*24*7));
		}

	}
}

//Le digo quiero o no DESCONECTARME
if(isset($_GET['desconectar'])){
	$_SESSION['conectado']=null;
	$_SESSION['usuario']=null;
	//Con esto BORRO la cookie del usuario
	setcookie('correoUsu', null, 0);
}

//Comprobamos si estamos o no conectados
if(isset($_SESSION['conectado'])){
	?>

	Bienvenido <?php echo $_SESSION['usuario']['nombreUsu'];?>
	(<?php echo $_SESSION['usuario']['correoUsu'];?>)
	<img src="img/usuarios/<?php echo $_SESSION['usuario']['imagenUsu'];?>" width="50">
	<br>
	<a href="index.php?desconectar=si">Desconectar</a>

	<?php
}else{
	?>
	<form action="index.php" method="post">
		<input type="text" name="usuario">
		<br>
		<input type="password" name="clave">
		<br>	
		Recordar usuario? <input type="checkbox" name="mantener">
		<br>
		<input type="submit" value="conectar" name="conectar">
	</form>
	<a href="index.php?p=registro.php">Darse de alta</a>
	<?php
}

?>