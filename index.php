<?php 
session_start();

include('includes/conexion.php');

if(isset($_GET['p'])){
  $p=$_GET['p'];
}else{
  $p='productos.php';
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Plantilla de Bootstrap</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">

  </head>
  <body>
    <section class="container">
      <header class="page-header" id="encabezado">
       <?php include('includes/encabezado.php'); ?>
       <br>
       <?php include('includes/login.php'); ?>
      </header>
      <nav>
        <?php include('includes/menu.php'); ?>
      </nav>
      <section>
        <?php include('paginas/'.$p); ?>
      </section>
      <footer>
        <?php include('includes/pie.php'); ?>
      </footer>
    </section>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>